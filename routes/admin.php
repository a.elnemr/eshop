<?php

use Illuminate\Support\Facades\Route;


Route::get('/admin/login', 'Auth\AdminController@adminLoginPage')
    ->name('admin.login-page');

Route::post('/admin/login', 'Auth\AdminController@login')
    ->name('admin.login');


Route::get('/admin/register', 'Auth\AdminController@adminRegsiterPage')
    ->name('register');

Route::middleware('auth:admin')->get('/admin', function () {
    dd(auth('admin')->user());
});
