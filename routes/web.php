<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('guest-user')->group(function () {
    Route::get('/login', 'Laravel\Fortify\Http\Controllers\AuthenticatedSessionController@create')
        ->name('login');

    Route::get('/register', 'Laravel\Fortify\Http\Controllers\RegisteredUserController@create')
        ->name('register');
});


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


Route::get('locale/{lang}', function ($lang) {
    session()->put('locale', $lang);
    return redirect(route('dashboard'));
})->name('locale');
