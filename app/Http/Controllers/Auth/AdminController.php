<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminAuthRequest;
use App\Jobs\SendAdminEmailJob;
use App\Mail\AdminLogged;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    public function login(AdminAuthRequest $request)
    {
        $data = $request->only(['email', 'password']);

        if (auth('admin')->attempt($data)) {
            $request->session()->regenerate();

            Mail::to(Admin::all())
                ->queue(new AdminLogged());

            return redirect('/admin');
        }
    }

    public function adminLoginPage()
    {
        return view('auth.admin.login');
    }
}
